﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Exercise.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPageCoba : ContentPage
	{
		public LoginPageCoba ()
		{
			InitializeComponent ();

            var createNewAccountTap = new TapGestureRecognizer();
            createNewAccountTap.Tapped += (s, e) =>
            {
                //do something
            };
            createAccountLabel.GestureRecognizers.Add(createNewAccountTap);

            var needHelpTap = new TapGestureRecognizer();
            needHelpTap.Tapped += (s, e) =>
            {
                //do something
                DisplayAlert("Help", "Silahkan bla bla bla", "ok");
            };
            helpLabel.GestureRecognizers.Add(needHelpTap);
		}
        

        private void Login_Clicked(object sender, EventArgs e)
        {
            string userName = email.Text;
            string pass = password.Text;

            //do something
        }
    }
}